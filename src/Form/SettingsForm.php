<?php

namespace Drupal\scheduler_request_cron\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form for setting Scheduler Request Cron settings .
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'scheduler_request_cron_settingsForm';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'scheduler_request_cron.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('scheduler_request_cron.settings');

    // Add a number field for interval.
    $form['interval'] = [
      '#type' => 'number',
      '#title' => $this->t('Interval in minutes'),
      '#default_value' => $config->get('interval'),
      '#min' => 1,
    ];

    // Add a checkbox for log setting.
    $form['log'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Logging'),
      '#default_value' => $config->get('log'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $interval = $form_state->getValue('interval');

    // Interval needs to be numeric for calculations.
    if (!is_numeric($interval)) {
      $form_state->setErrorByName('interval', $this->t('The interval needs to a integer.'));
    }

    // Interval needs to be bigger than one to prevent too many executions.
    if ($interval < 1) {
      $form_state->setErrorByName('interval', $this->t('The interval needs to be at least 1 minute.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration and set new values.
    $this->configFactory->getEditable('scheduler_request_cron.settings')
      ->set('interval', (int) $form_state->getValue('interval'))
      ->set('log', (bool) $form_state->getValue('log'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
